﻿using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float range = 100f; //koliko daleko metak ide


    float timer; //meri razmak izmedju 2 pucnja
    Ray shootRay = new Ray(); //zrak gde se puca
    RaycastHit shootHit;
    int shootableMask; //da znamo da mozemo samo shootable stvari dda pucamo
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;


    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable"); 
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();
    }


    void Update ()
    {
        timer += Time.deltaTime;

		if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            Shoot ();
        }

        if(timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects ();
        }
    }


    public void DisableEffects ()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    void Shoot ()
    {
        timer = 0f;

        gunAudio.Play ();

        gunLight.enabled = true;

        gunParticles.Stop (); //da ne bi bio replay, nego pukne pa stane p apukne pa stane..
        gunParticles.Play ();

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position); //skripta je na barrel pa je to  pocetna pozicija pucnja
        //pa racunamo drugu poziciju

        shootRay.origin = transform.position; //zrak pocinje od barrel-a
        shootRay.direction = transform.forward; //i ide u nekom smeru, napred

        //koristimo fiziku za pucnjeve
        //kada dodje do pucnja on moze da ide najvise 100units-a i kada god udari u nesto vrati se nama info o tom objektu i kazemo to smo udarili
        //i crtamo liniju, zrak
        //ako ne udarimo ni u sta, ne vraca nam se info o objektu i kazemo mnogo daleko udara, pa crtamo samo veoma dugacku liniju

        //sootRay - that way
        //shootHit - what we hit
        //range - 100 units
        //shootableMask - ovaj zrak moze da pogodi samo stvari za koje smo rekli da su shootable(LAYOUT) 
        if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
        {
            //sta god da sam pogodio, daj mi enemy scriptu za njega da smanjim health
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
            //ako pogodimo zid, sto.... nema skriptu pa je null
            //ukoliko nije null i enemy je, smanji mu health (public fja)
            if(enemyHealth != null)
            {
                enemyHealth.TakeDamage (damagePerShot, shootHit.point);
            }
            //svakako stavi drugu poziciju za zrak na tacku koja je pogodjena, 1 je second point, 0 je first point
            gunLine.SetPosition (1, shootHit.point);
        }
        else
        {
            //ako nikog ne pogodimo
            //second point je uvecana, i to udaljenost originalne pozicije zraka uvecana za smer zraka i sve to * rang koji je 100 units-a
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
    }
}
