﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public GameObject enemy;
    public float spawnTime = 3f;
    public Transform[] spawnPoints; //niz kako bi imali vise enemija odjednom i vodimo racuna o njima
    //kopija skripte za svakog enemija posebno i spawn points (obican game object sa pozicijom) podesen da odatle dolazi enemy


    void Start ()
    {
        InvokeRepeating ("Spawn", spawnTime, spawnTime); //na spawn time vreme se izvrsava iznova spawn fja odozdo
    }


    void Spawn ()
    {
        if(playerHealth.currentHealth <= 0f)
        {
            return;
        }
        //alp nije mrtav player
        int spawnPointIndex = Random.Range (0, spawnPoints.Length); //ma random mesto izlazi
        //instanciramo enemija
        Instantiate (enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }
}
