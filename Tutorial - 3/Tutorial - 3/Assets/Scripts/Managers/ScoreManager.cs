﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score; //static nije za instancu vec samo za klasu, unutar klase se koristi

    //mozemo da imamo vise instanci score managera ali svi ce oni imati isti score


    Text text;


    void Awake ()
    {
        text = GetComponent <Text> ();
        score = 0;
    }


    void Update ()
    {
        text.text = "Score: " + score;
    }
}
